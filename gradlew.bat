apply plugin: 'com.android.application'
apply plugin: 'realm-android'

android {
    compileSdkVersion 28
    defaultConfig {
        applicationId "com.example.posvitmobile"
        minSdkVersion 23
        targetSdkVersion 28
        versionCode 15
        versionName "2.2.1"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
        ndk {
            abiFilters "armeabi", "armeabi-v7a", "x86", "mips"
        }
        multiDexEnabled true
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    compile fileTree(dir: "$buildDir/native-libs", include: 'native-libs.jar')
    compile fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'com.android.support:appcompat-v7:28.0.0'
    implementation 'com.android.support.constraint:constraint-layout:1.1.3'
    implementation 'com.android.support:design:28.0.0'
    implementation 'com.theartofdev.edmodo:android-image-cropper:2.7.0'
    implementation 'id.zelory:compressor:2.1.0'
    implementation 'com.toptoche.searchablespinner:searchablespinnerlibrary:1.3.1'
    implementation 'com.jakewharton:butterknife:8.8.1'
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'
    implementation 'com.squareup.picasso:picasso:2.5.2'
    implementation 'de.hdodenhof:circleimageview:2.2.0'
    implementation 'me.grantland:autofittextview:0.2.+'
    implementation 'com.journeyapps:zxing-android-embedded:3.6.0'
    implementation 'com.google.zxing:core:3.3.0'
    implementation 'com.hopenlib.library:cflextools:1.0.0'
    implementation 'com.google.android:flexbox:0.3.2'
    implementation 'com.rengwuxian.materialedittext:library:2.1.4'
    implementation 'com.squareup.retrofit2:converter-gson:2.2.0'
    implementation 'com.squareup.retrofit2:retrofit:2.2.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.4.1'
    implementation 'com.squareup.okhttp3:okhttp:3.4.1'
    implementation 'pl.tajchert:waitingdots:0.2.0'
    //    implementation 'com.google.android.gms:play-services-maps:16.1.0'
    //    implementation 'com.go