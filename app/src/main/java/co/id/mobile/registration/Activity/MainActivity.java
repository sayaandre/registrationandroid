package co.id.mobile.registration.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import co.id.mobile.registration.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
