package co.id.mobile.registration.Helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManagement {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    public static final String KEY_NO_KTP = "keyNoKTP";
    public static final String KEY_NAMA = "keyNama";
    public static final String KEY_NOREK = "keyNorek";
    public static final String KEY_EDU = "keyEdu";
    public static final String KEY_DOB = "keyDOB";
    public static final String KEY_ALAMAT = "keyAlamat";
    public static final String KEY_RESIDENCE = "keyResidence";
    public static final String KEY_BLOK = "keyBlok";
    public static final String KEY_PROVINCE = "keyProvince";

    int PRIVATE_MODE = 0;

    public SessionManagement(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("RegistrationTest", PRIVATE_MODE);
        editor = pref.edit();
    }

    public String getKeyNoKtp() {
        return pref.getString(KEY_NO_KTP, "");
    }

    public String getKeyNama() {
        return pref.getString(KEY_NAMA, "");
    }

    public String getKeyNorek() {
        return pref.getString(KEY_NOREK, "");
    }

    public String getKeyEdu() {
        return pref.getString(KEY_EDU, "");
    }

    public String getKeyDob() {
        return pref.getString(KEY_DOB, "");
    }

    public String getKeyAlamat() {
        return pref.getString(KEY_ALAMAT, "");
    }

    public String getKeyResidence() {
        return pref.getString(KEY_RESIDENCE, "");
    }

    public String getKeyBlok() {
        return pref.getString(KEY_BLOK, "");
    }

    public String getKeyProvince() {
        return pref.getString(KEY_PROVINCE, "");
    }

    public void setDataDiri(String noktp, String nama, String norek, String edu, String tgl)
    {
        editor.putString(KEY_NO_KTP, noktp);
        editor.putString(KEY_NAMA, nama);
        editor.putString(KEY_NOREK, norek);
        editor.putString(KEY_EDU, edu);
        editor.putString(KEY_DOB, tgl);
        editor.commit();
    }

    public void setAlamatKTP(String alamat, String residence, String blok, String provinsi)
    {
        editor.putString(KEY_ALAMAT, alamat);
        editor.putString(KEY_RESIDENCE, residence);
        editor.putString(KEY_BLOK, blok);
        editor.putString(KEY_PROVINCE, provinsi);
        editor.commit();
    }

    public void clearAll() {
        editor.putString(KEY_NO_KTP, "");
        editor.putString(KEY_NAMA, "");
        editor.putString(KEY_NOREK, "");
        editor.putString(KEY_EDU, "");
        editor.putString(KEY_DOB, "");
        editor.putString(KEY_ALAMAT, "");
        editor.putString(KEY_RESIDENCE, "");
        editor.putString(KEY_BLOK, "");
        editor.putString(KEY_PROVINCE, "");
        editor.commit();
    }
}
