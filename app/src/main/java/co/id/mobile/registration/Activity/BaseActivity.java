package co.id.mobile.registration.Activity;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import co.id.mobile.registration.Core.CoreApplication;
import co.id.mobile.registration.Helper.SessionManagement;
import co.id.mobile.registration.R;

public class BaseActivity extends AppCompatActivity {

    SessionManagement session;
    ProgressDialog progressDialog;
    boolean isTablet = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = CoreApplication.getInstance().getSession();
    }

    public void dismissProgressDialog() {
        if(!isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }
}
