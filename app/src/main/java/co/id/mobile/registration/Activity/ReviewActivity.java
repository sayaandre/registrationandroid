package co.id.mobile.registration.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import co.id.mobile.registration.R;

public class ReviewActivity extends BaseActivity {

    TextView txtKTP, txtNama, txtNoRek, txtEdu, txtDOB, txtAlamat, txtResidence, txtBlok, txtProvince;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        setTitle("Review");
        txtKTP = findViewById(R.id.editNoKTP);
        txtNama = findViewById(R.id.editNama);
        txtNoRek = findViewById(R.id.editNoRek);
        txtEdu = findViewById(R.id.editEdu);
        txtDOB = findViewById(R.id.editDOB);
        txtAlamat = findViewById(R.id.editAlamat);
        txtResidence = findViewById(R.id.editResidence);
        txtBlok = findViewById(R.id.editBlok);
        txtProvince = findViewById(R.id.editProvince);
        setDisplay();
    }

    public void setDisplay()
    {
        txtKTP.setText(session.getKeyNoKtp());
        txtNama.setText(session.getKeyNama());
        txtNoRek.setText(session.getKeyNorek());
        txtEdu.setText(session.getKeyEdu());
        txtDOB.setText(session.getKeyDob());
        txtAlamat.setText(session.getKeyAlamat());
        txtResidence.setText(session.getKeyResidence());
        txtBlok.setText(session.getKeyBlok());
        txtProvince.setText(session.getKeyProvince());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
