package co.id.mobile.registration.Rest.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProvinceResponse {
    @SerializedName("error")
    @Expose
    private boolean error;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("semuaprovinsi")
    @Expose
    private List<GetDetailProvince> semuaprovinsi;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetDetailProvince> getSemuaprovinsi() {
        return semuaprovinsi;
    }

    public void setSemuaprovinsi(List<GetDetailProvince> semuaprovinsi) {
        this.semuaprovinsi = semuaprovinsi;
    }
}
