package co.id.mobile.registration.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.id.mobile.registration.R;
import co.id.mobile.registration.Rest.ApiClient;
import co.id.mobile.registration.Rest.ApiService;
import co.id.mobile.registration.Rest.Response.GetProvinceResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlamatKTPActivity extends BaseActivity {

    Spinner spinnerResidence, spinnerProvince;
    EditText txtAlamat, txtBlok;
    Button btnSave;

    private String[] residenceName;
    private String[] provinceName;
    ApiService api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alamat_ktp);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        setTitle("Alamat KTP");
        api = ApiClient.getClient().create(ApiService.class);
        spinnerResidence = findViewById(R.id.spinnerResidence);
        spinnerProvince = findViewById(R.id.spinnerProvince);
        txtAlamat = findViewById(R.id.editAlamatKTP);
        txtBlok = findViewById(R.id.editBlok);
        residenceName = new String[2];
        residenceName[0] = "Rumah";
        residenceName[1] = "Kantor";
        final ArrayAdapter<String> adapterTemplates = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, residenceName);
        spinnerResidence.setAdapter(adapterTemplates);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate())
                {
                    session.setAlamatKTP(txtAlamat.getText().toString(), spinnerResidence.getSelectedItem().toString(),
                            txtBlok.getText().toString(), spinnerProvince.getSelectedItem().toString());
                    Intent i = new Intent(AlamatKTPActivity.this, ReviewActivity.class);
                    startActivity(i);
                }
            }
        });
        getProvince();
    }

    public void getProvince()
    {
        Call<GetProvinceResponse> call = api.getProvince();
        call.enqueue(new Callback<GetProvinceResponse>() {
            @Override
            public void onResponse(Call<GetProvinceResponse> call, final Response<GetProvinceResponse> response) {
                if(response.isSuccessful())
                {
                    provinceName = new String[response.body().getSemuaprovinsi().size()];
                    for(int i = 0 ; i < response.body().getSemuaprovinsi().size() ; i++)
                    {
                        provinceName[i] = response.body().getSemuaprovinsi().get(i).getNama();
                    }
                    final ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(AlamatKTPActivity.this,
                            android.R.layout.simple_spinner_item, provinceName);
                    spinnerProvince.setAdapter(adapterProvince);
                }
            }

            @Override
            public void onFailure(Call<GetProvinceResponse> call, Throwable t) {

            }
        });
    }

    private boolean validate()
    {
        boolean valid = true;
        String alamat = txtAlamat.getText().toString();
        String blok = txtBlok.getText().toString();
        if(alamat.isEmpty())
        {
            valid = false;
            txtAlamat.setError("Isi No KTP");
        }
        else
        {
            txtAlamat.setError(null);
        }
        if(blok.isEmpty())
        {
            valid = false;
            txtBlok.setError("Isi No Rekening");
        }
        else
        {
            txtBlok.setError(null);
        }
        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
