package co.id.mobile.registration.Core;

import android.app.Application;
import android.content.Intent;
import android.os.Process;

import co.id.mobile.registration.Activity.MainActivity;
import co.id.mobile.registration.Helper.SessionManagement;


public class CoreApplication extends Application {

    public static CoreApplication app;
    SessionManagement session;

    @Override
    public void onCreate() {
        super.onCreate();
        session = new SessionManagement(this);
        app = this;
    }

    public static CoreApplication getInstance() {
        return app;
    }

    public SessionManagement getSession() {
        return session;
    }

    public void closeDay()
    {
        session.clearAll();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void closeApplication() {
        Process.killProcess(Process.myPid());
    }
}
