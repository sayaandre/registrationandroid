package co.id.mobile.registration.Rest;

import co.id.mobile.registration.Rest.Response.GetDetailProvince;
import co.id.mobile.registration.Rest.Response.GetProvinceResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    @GET("daerahindonesia/provinsi")
    Call<GetProvinceResponse> getProvince();
}
