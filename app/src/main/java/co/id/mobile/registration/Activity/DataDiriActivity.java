package co.id.mobile.registration.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.id.mobile.registration.Helper.Tools;
import co.id.mobile.registration.R;

public class DataDiriActivity extends BaseActivity {

    Spinner spinnerEdu;
    EditText txtNoKTP, txtNama, txtNorek, txtDOB;
    Button btnSave;

    private String[] eduName;
    String date_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_diri);
        ActionBar actionbar = getSupportActionBar();
        setTitle("Data Diri");
        spinnerEdu = findViewById(R.id.spinnerEdu);
        txtNoKTP = findViewById(R.id.editNoKTP);
        txtNama = findViewById(R.id.editNama);
        txtNorek = findViewById(R.id.editNorek);
        txtDOB = findViewById(R.id.txtDOB);
        btnSave = findViewById(R.id.btnSave);
        eduName = new String[6];
        eduName[0] = "SD";
        eduName[1] = "SMP";
        eduName[2] = "SMA";
        eduName[3] = "S1";
        eduName[4] = "S2";
        eduName[5] = "S3";
        final ArrayAdapter<String> adapterTemplates = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, eduName);
        spinnerEdu.setAdapter(adapterTemplates);
        txtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.dismissKeyboard(DataDiriActivity.this);
                datePicker();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate())
                {
                    session.setDataDiri(txtNoKTP.getText().toString(), txtNama.getText().toString(),
                            txtNorek.getText().toString(), spinnerEdu.getSelectedItem().toString(),
                            txtDOB.getText().toString());
                    Intent i = new Intent(DataDiriActivity.this, AlamatKTPActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private boolean validate()
    {
        boolean valid = true;
        String noktp = txtNoKTP.getText().toString();
        String norek = txtNorek.getText().toString();
        String dob = txtDOB.getText().toString();
        if(noktp.isEmpty())
        {
            valid = false;
            txtNoKTP.setError("Isi No KTP");
        }
        else
        {
            txtNoKTP.setError(null);
        }
        if(norek.isEmpty())
        {
            valid = false;
            txtNorek.setError("Isi No Rekening");
        }
        else
        {
            txtNorek.setError(null);
        }
        if(dob.isEmpty())
        {
            valid = false;
            txtDOB.setError("Isi Tanggal Lahir");
        }
        else
        {
            txtDOB.setError(null);
        }
        return valid;
    }

    private void datePicker(){
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if(view.isShown()) {
                            c.set(year, monthOfYear, dayOfMonth);
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            date_time = format.format(c.getTime());
                            txtDOB.setText(date_time);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
